 import ProductCard from "../../components/ProductCard";

// export const getStaticPaths= async () =>{
// const res = await fetch("https://fakestoreapi.com/products");
// const data = await res.json();

// const paths=data.map((product: { id: { toString: () => void; }; }) =>{
//   return{
//     params:{
//       productid:product.id.toString(),
//     },
//   };
// });

// return{
//   paths,
//   fallback:false,
// };
// };

// export const getStaticProps= async(context: { params: { productid: any; }; }) => {
//   const id =context.params.productid;
//   const res=await fetch('https://fakestoreapi.com/products/${id}');
// const data=await res.json();
//   return {
//   props: {
//   data,
//   },
// };
// };

// const myData =({data})=>{
//   return(
//     <>
//     <ProductCard/>
//      <div key={data.id}>
//       <h3>{data.id}</h3>
//       <h2>{data.price}</h2>
//       <p>{data.description}</p>
//     </div>
//     </>
//   );

// };

// export default myData


export async function getStaticProps({params}){
  const results =await fetch(`https://fakestoreapi.com/products/${params.productId}`).then(r=>r.json());
  return{
    props:{
      products:results[0]
    }
  }
}

export async function getStaticPaths(){
  const products =await fetch('https://fakestoreapi.com/products').then(r=>r.json());
  return{
    paths:products.map((product: any)=>{
      return{
        params:{
          productId:product.name
        }
      }
    }),
    fallback:false
  }
}


